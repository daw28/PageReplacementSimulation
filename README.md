# Page Replacement Algorithm Simulation

The code in this repo was written to satisfy Lab4 of COMP301 at the University of Waikato.

The simulation keeps track of a simple table of 8 virtual pages which can be mapped onto 4 physical page frames. The input files contain 2 numbers on each line seperated by a space. The first is the next page number to be read from and the other, a one or zero, indicates if that page will be modified.

The simulation counts how many page faults occur and how many times the disk is accessed (reads and writes) and writes its results to a file.

# Usage

`./lab4.exe [in.file] [out.file]` where in.file is an input file, out.file is the name of the generated report file.

## Algorithms

* Random Replacement  (RND) - on page fault, evict a random page.
* Not Recently Used   (NRU) - on page fault, evict random page from lowest class (class determined by referenced and modification bits)
* Least Recently Used (LRU) - on page fault, evict the page that has been accessed the lease (keeps count of page accessess)
* First in, First out (FIF) - on page fault, evict the page that was loaded in first.
* Second Chance       (SEC) - on page fault, evict the page that was loaded in first if it is not referenced. If the page has been referenced, clear the refernce bit and put to back of queue.

## Compilation

The executable can be compiled using GNU make. Simply typing `make` in the root directory will compile the source using gcc.
