#include <stdlib.h>

// Simple structure and two methods to form the queue
typedef struct listNode_st
{
    int page;
    struct listNode_st* next;
} listNode;

void add(listNode** root, int page);
int pop(listNode** root);
int contains(listNode* root, int value);
