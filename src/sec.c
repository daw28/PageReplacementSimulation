#include "page.h"
#include "queue.h"

listNode* root;
listNode* last;

void sec(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable )
{
    if(*count == 0) // initialize list on first invocation
        root = 0;

    *nMemRef += 1;
    pageEntry *entry = &pageTable[page];
    int frame;
    entry->referenced = true;

    /* On Page Fault */
    if(!entry->present)
    {
        *nFaults += 1;
        if(*count == NUM_FRAMES) // memory is full! envoke algorithm
        {
            int evPage;
            while(1)    // find first unreferenced page
            {
                evPage = pop(&root);
                if(pageTable[evPage].referenced)
                {
                    pageTable[evPage].referenced = false;
                    add(&root, evPage);
                }
                else
                {
                    break;
                }
            }

            int evFrame;
            for (int i = 0; i < NUM_FRAMES; i++ )
            {
                if(evPage == pageFrames[i])
                {
                    frame = i;
                    break;
                }
            }
            pageEntry* evEntry = &pageTable[evPage];
            evEntry->present = false;
            if(evEntry->modified)
            {
                *nDiskRef += 1; // one disk ref to save file
            }
        }
        else
        {
            frame = (*count)++;
        }

        *nDiskRef += 1; // one disk ref to read file
        entry->present = true;
        entry->modified = modified;
        entry->frame = frame;

        pageFrames[frame] = page;
        add(&root, page);
    }
    else // page already in physical memory
    {
        entry->modified = entry->modified || modified;
    }
}
