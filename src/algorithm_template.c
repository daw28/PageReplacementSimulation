#include "page.h"

void alg(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable )
{
    *nMemRef += 1;
    pageEntry *entry = &pageTable[page];
    int frame;
    entry->referenced = true;

    /* On Page Fault */
    if(!entry->present)
    {
        *nFaults += 1;
        if(*count == NUM_FRAMES) // memory is full! envoke algorithm
        {

        }
        else
        {
            frame = (*count)++;
        }

        *nDiskRef += 1; // one disk ref to read file
        entry->present = true;
        entry->modified = modified;
        entry->frame = frame;

        pageFrames[frame] = page;
    }
    else // page already in physical memory
    {
        entry->modified = entry->modified || modified;
    }
}
