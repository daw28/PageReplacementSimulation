#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "algorithms.h"


#define SEED 2017

pageEntry pageTable[NUM_PAGES];
int pageFrames[NUM_FRAMES];

// Print algorithm results to report file
void printAlgorithmReport(FILE* reportFile, algorithm* alg )
{
    fprintf(reportFile, "%s M %d F %d D %d L %d %d %d %d\n", \
        alg->name, alg->nMemRef, alg->nFaults, alg->nDiskRef, \
        pageFrames[0], pageFrames[1], pageFrames[2], pageFrames[3] );
}

// Run the simulation (one for now)
void simulate(char* fRefPages, char* fReport, algorithm* alg, int num_algorithms)
{
    FILE* fpRefPages = fopen(fRefPages, "r");
    FILE* fpReport = fopen(fReport, "w");
    int page, modified;
    srand(SEED);

    for(int i = 0; i < num_algorithms; i++)
    {

        // wipe memory for each simulation
        memset(pageFrames, -1, sizeof(int) * NUM_FRAMES);
        memset(pageTable, 0, sizeof(pageEntry) * NUM_PAGES );

        while( fscanf( fpRefPages, "%d %d", &page, &modified ) != EOF )
        {
            alg[i].replacePage(page, modified, \
                &alg[i].count, &alg[i].nFaults, &alg[i].nMemRef, &alg[i].nDiskRef, \
                pageFrames, pageTable );
        }

        printAlgorithmReport(fpReport, &alg[i]);

        rewind(fpRefPages);     // rewind input FILE

    }

    fclose(fpRefPages);
    fclose(fpReport);
}

int main(int argc, char** argv){
    simulate(argv[1], argv[2], algorithms, NUM_ALGORITHMS);
    return 0;
}
