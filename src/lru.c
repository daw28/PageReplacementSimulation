#include "page.h"

int counters[NUM_FRAMES];

void init()
{
    for (int i = 0; i < NUM_FRAMES; i++ )
    {
        counters[i] = 0;
    }
}

void lru(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable )
{
    if(*count == 0) // initialize counters on first invocation
        init();

    *nMemRef += 1;
    pageEntry *entry = &pageTable[page];
    int frame;
    entry->referenced = true;

    /* On Page Fault */
    if(!entry->present)
    {
		// perform lru algorithm here
		/*  keeps track of number of accesses to each page in memory
		 *	on page fault removes page with lowest number of accesses
		 */
		*nFaults += 1;
        if(*count == NUM_FRAMES) // memory is full! envoke algorithm
        {
            int lowest = 10000;
            int evFrame = 0;
            for (int i = 0; i < NUM_FRAMES; i++ )
            {
                if( counters[i] <= lowest )
                {
                    evFrame = i;
					lowest = counters[i];
                }
            }

            int evPage = pageFrames[evFrame];
            pageEntry* evEntry = &pageTable[evPage];
            evEntry->present = false;
            frame = evFrame;
            if(evEntry->modified)
            {
                *nDiskRef += 1; // one disk ref to save file
            }
        }
        else
        {
            frame = (*count)++;
        }

        *nDiskRef += 1; // one disk ref to read file
        entry->present = true;
        entry->modified = modified;
        entry->frame = frame;

        pageFrames[frame] = page;

        counters[frame] = 0; // reset counter for this frame (and add this reference)
    }
    else // page already in physical memory
    {
        for (int i = 0; i < NUM_FRAMES; i++ )
        {
            if(page == pageFrames[i])
                counters[i] += 1;
        }
        entry->modified = entry->modified || modified;
    }
}
