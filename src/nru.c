#include "page.h"

void nru(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable )
{
    *nMemRef += 1;
    pageEntry *entry = &pageTable[page];
    int frame;
    entry->referenced = true;

    /* On Page Fault */
    if(!entry->present)
    {
        *nFaults += 1;
        if(*count == NUM_FRAMES) // memory is full! envoke algorithm
        {
            // perform nru algorithm here
            /*  replace lowest numbered non-modified page (class 2)
             *  if none - replace first page (lowest numbered modified page
             *            when all pages are modified)
             */
            int replaced = 0;
            for(int i = 0; i < NUM_FRAMES; i++)
            {
                if(!pageTable[pageFrames[i]].modified)
                {
                    pageEntry* evEntry = &pageTable[pageFrames[i]];
                    evEntry->present = false;
                    frame = i;
                    replaced = 1;
                    break;
                }
            }
            if(!replaced) // if that didn't do it
            {
                pageEntry* evEntry = &pageTable[pageFrames[0]];
                evEntry->present = false;
                frame = 0;
            }
        }
        else
        {
            frame = (*count)++;
        }

        *nDiskRef += 1; // one disk ref to read file
        entry->present = true;
        entry->modified = modified;
        entry->frame = frame;

        pageFrames[frame] = page;
    }
    else // page already in physical memory
    {
        entry->modified = entry->modified || modified;
    }
}
