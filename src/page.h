#define NUM_PAGES 8
#define NUM_FRAMES 4

enum {false, true};
typedef struct pageEntry_st
{
    int frame;
    int referenced;
    int modified;
    int present;
} pageEntry;
