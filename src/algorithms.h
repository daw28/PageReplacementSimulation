#include "page.h"
typedef void (*pageReplacement)(int, int, int*, int*, int*, int*, int*, pageEntry* );
typedef struct algorithm_st
{
    char* name;             // Algorithm name for output
    pageReplacement replacePage;    // Algorithm implementation function pointer

    int count, nFaults, nMemRef, nDiskRef; // simulation stats
} algorithm;

// algorithm function definitions
void rnd(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable );
void nru(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable );
void lru(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable );
void fif(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable );
void sec(int page, int modified, int* count, int* nFaults, int* nMemRef, int* nDiskRef, int* pageFrames, pageEntry* pageTable );

#define NUM_ALGORITHMS 5

algorithm algorithms[] = {
    {"NRU", nru, 0, 0, 0, 0 },
    {"RND", rnd, 0, 0, 0, 0 },
    {"FIF", fif, 0, 0, 0, 0 },
    {"SEC", sec, 0, 0, 0, 0 },
    {"LRU", lru, 0, 0, 0, 0 }
};
