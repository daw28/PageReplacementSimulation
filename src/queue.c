#include <stdio.h>
#include "queue.h"

void add(listNode** root, int page)
{
    listNode* newNode = (listNode*)malloc(sizeof(listNode));
    newNode->page = page;
    newNode->next = 0;
    if(*root == 0)
    {
        *root = newNode;
    }
    else
    {
        listNode* node = *root;
        while(node->next != 0)
            node = node->next;
        node->next = newNode;
    }
}

int pop(listNode** root)
{
    if(*root == 0)
    {
        printf("Uh-oh, pop called on empty queue :( \n");
        return 0;
    }
    int value = (*root)->page;
    listNode* tmp = *root;
    *root = (*root)->next;
    free(tmp);
    return value;
}

int contains(listNode* root, int value)
{
    while(root != 0)
    {
        if(root->page == value)
            return 1;
        root = root->next;
    }
    return 0;
}
